<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;

class DnShopClass extends Model
{
    protected $table = 'tb_shop_class';

    protected $connection = 'mysql_dn';

    public $timestamps = FALSE;

    protected $primaryKey = 'class_id';
}
