<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Validator;

class DnShopGood extends Model
{
    use ValidatesRequests;
    //
    protected $table = 'tb_shop_good';
    protected $connection = 'mysql_dn';
    protected $primaryKey = "good_id";
    protected $rules = [];
    protected $message = [];

    public $timestamps = FALSE;
    protected $fillable = [
        'good_id', 'good_photo', 'good_price', 'good_marketprice', 'good_content', 'class_id',
        'sort', 'good_stock', 'good_name'
    ];
    protected $appends = ['className'];
    //    protected $hidden = ['path'];

    public $errors;

    public function validate($data)
    {
        return true;
    }


    public function getClassNameAttribute()
    {
        return '分类名';
    }

    /**
     * 模型关联获取资源方表
     *
     * @return void
     */
    public function shopClass()
    {
        return $this->belongsTo('App\Models\DnShopClass', 'class_id', 'class_id')->withDefault([
            'class_name' => '',
        ]);
    }
}
