<?php

namespace App\Http\Requests\Dn;

use App\Http\Requests\Request;

class ShopClassRequest extends Request
{
    public function rules()
    {
        switch ($this->method()) {
                // CREATE
            case 'POST':
                // UPDATE
            case 'PUT':
            case 'PATCH': {
                    return [
                        'class_name' => 'required|string|max:255',
                        'class_photo' => 'required|string|max:255',
                        'sort' => '',
                    ];
                }
            case 'GET':
            case 'DELETE':
            default: {
                    return [];
                };
        }
    }

    public function messages()
    {
        return [];
    }
}
