<?php

namespace App\Http\Controllers\Dn;

use App\Http\Controllers\Controller;

use App\Models\Activity;
use App\Models\Article;
use App\Models\DnArticleClass;
use App\Models\DnBanner;
use App\Models\DnClockClass;
use App\Models\DnClockMainClass;
use App\Models\DnClockRecord;
use App\Models\DnClockRecordClass;
use App\Models\DnClockThreeClass;
use App\Models\DnShopGood;
use App\Models\DnShopOrder;
use App\Models\DnUser;
use App\Models\DnUserClass;
use App\Models\DnUserCommunity;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    //测试
    public function test()
    {

        echo 'hello world';
        exit;
    }

    public function options()
    {
        return $this->jsonOk([]);
    }


    public function order_list(Request $request)
    {

        $page = $request->get('page', 1);
        $pageSize = $request->get('limit', 20);
        $offset = ($page - 1) * $pageSize;

        //处理排序
        $order = $request->get('sort', '-time');
        $desc = strrpos('-', $order) === false ? 'asc' : 'desc';

        //筛选
        $type = $request->get('type', '');
        $query = DnShopOrder::query();
        if ($type) {
            $query->where('type', $type);
        }
        $list = $query->offset($offset)->limit($pageSize)->orderBy(ltrim($order, '-'), $desc)
            ->get();
        $count = DnShopOrder::query()->count();

        return $this->jsonOk(['list' => $list, 'total' => $count]);
    }

    public function order_delete(Request $request)
    {
        $id = $request->get('id');
        DnShopOrder::query()->where('id', $id)->delete();
        return $this->jsonOk([], '删除成功');
    }

    public function order_update(Request $request)
    {
        $id = $request->get('id');
        $model = DnShopOrder::query()->where('id', $id)->first();
        if ($model == null) {
            return $this->jsonErr([], '未找到此文章，id:' . $id);
        }
        $model->fill($request->all());
        if (!$model->save()) {
            throw new \RuntimeException("更新失败");
        }
    }

    public function shop_order_status_update(Request $request)
    {
        $id = $request->get('order_id');
        $model = DnShopOrder::query()->where('order_id', $id)->first();
        if ($model == null) {
            return $this->jsonErr([], '未找到此订单，id:' . $id);
        }
        $model->zt ^= 1;
        if (!$model->save()) {
            throw new \RuntimeException("更新失败");
        }
        return $this->jsonOk([], '更新成功');
    }


    public function good_list(Request $request)
    {

        $page = $request->get('page', 1);
        $pageSize = $request->get('limit', 20);
        $offset = ($page - 1) * $pageSize;

        //处理排序
        $order = $request->get('sort', '-good_id');
        $desc = strrpos('-', $order) === false ? 'asc' : 'desc';

        //筛选
        $type = $request->get('type', '');
        $query = DnShopGood::with('shopClass');
        if ($type) {
            $query = $query->where('type', $type);
        }
        $count = $query->count();
        $list = $query->offset($offset)->limit($pageSize)->orderBy(ltrim($order, '-'), $desc)
            ->get();

        return $this->jsonOk(['list' => $list, 'total' => $count]);
    }

    public function good_create(Request $request)
    {
        $model = new DnShopGood($request->all());



        if ($model->validate($request->all())) {
            $model->class_id = 0;
            $model->good_photo = $request->input('cover', '');
            if (!$model->save()) {
                throw new \RuntimeException("插入失败");
            }
            return $this->jsonOk($model, '添加成功');
        } else {
            $message = $model->errors[0] ?? '位置错误';
            return $this->jsonErr([], '添加失败！' . $message);
        }
    }

    public function good_detail(Request $request)
    {
        $id = $request->get('id', 0);
        $model = DnShopGood::query()
            ->where('good_id', $id)
            ->first();
        if ($model == null) {
            $message = "未找到此文章";
            return $this->jsonErr(80000, $message);
        } else {
            return $this->jsonOk($model, '');
        }
    }

    public function good_update(Request $request)
    {
        $id = $request->get('good_id');
        $model = DnShopGood::query()->where('good_id', $id)->first();
        if ($model == null) {
            return $this->jsonErr([], '未找到此文章，id:' . $id);
        }
        $model->fill($request->all());
        $model->good_photo = $request->input('cover', '');
        $model->good_content = $request->input('good_content', '');
        if (!$model->save()) {
            throw new \RuntimeException("更新失败");
        }
        return $this->jsonOk($model, '更新成功');
    }

    public function good_delete(Request $request)
    {
        $id = $request->get('id');
        DnShopGood::query()->where('good_id', $id)->delete();
        return $this->jsonOk([], '删除成功');
    }

    public function good_class_list(Request $request)
    {



        return $this->jsonOk(['list' => [['id' => 1, 'name' => "fdsaf"], ['id' => 2, 'name' => "dasfa"]], 'total' => 2]);
    }
}
