<?php
/**
 * Created by PhpStorm.
 * User: fredgui
 * Date: 2021/6/19
 * Time: 14:37
 */
Route::get('dn/shoptest', ['uses' => 'Dn\ShopController@test']);


Route::get('dn/shop_order', ['uses' => 'Dn\ShopController@order_list']);
Route::get('dn/shop_order_detail', 'Dn\ShopController@shop_order_detail');
Route::post('dn/shop_order_update', ['uses' => 'Dn\ShopController@shop_order_update']);



Route::get('dn/good', ['uses' => 'Dn\ShopController@good_list']);
Route::get('dn/good_detail', ['uses' => 'Dn\ShopController@good_detail']);
Route::post('dn/good_create', ['uses' => 'Dn\ShopController@good_create']);
Route::post('dn/good_delete', ['uses' => 'Dn\ShopController@good_delete']);
Route::post('dn/good_update', ['uses' => 'Dn\ShopController@good_update']);
Route::post('dn/shop_order_status_update', ['uses' => 'Dn\ShopController@shop_order_status_update']);



Route::get('dn/good_class_list', ['uses' => 'Dn\ShopController@good_class_list']);